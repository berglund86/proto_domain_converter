package org.silbertb.proto.domainconverter.builder.domain;

import lombok.*;
import org.silbertb.proto.domainconverter.annotations.*;
import org.silbertb.proto.domainconverter.basic.domain.PrimitiveDomain;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofBaseFieldDomain;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofIntImplDomain;
import org.silbertb.proto.domainconverter.test.proto.builder.AllInOneBuilderConstructorProto;

import java.util.HashMap;
import java.util.List;


@Data
@ProtoClass(protoClass = AllInOneBuilderConstructorProto.class)
public class AllInOneBuilderConstructorDomain {

    @Builder
    @ProtoBuilder
    private AllInOneBuilderConstructorDomain(byte[] bytesVal, HashMap<String, String> mapVal, List<String> listVal,
                                            @OneofBase(oneOfFields = {
                                                    @OneofField(protoField = "oneof_int_val", domainClass = OneofIntImplDomain.class, domainField = "intVal"),
                                                    @OneofField(protoField = "oneof_primitives", domainClass = PrimitiveDomain.class)
                                            })
                                            OneofBaseFieldDomain value) {
        this.bytesVal = bytesVal;
        this.mapVal = mapVal;
        this.listVal = listVal;
        this.value = value;
    }

    @ProtoField
    private String strVal;
    private final byte[] bytesVal;
    private final HashMap<String, String> mapVal;
    private final List<String> listVal;
    private final OneofBaseFieldDomain value;
}
