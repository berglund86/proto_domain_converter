package org.silbertb.proto.domainconverter.blacklist;

import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.blacklist.domain.BlacklistDomain;
import org.silbertb.proto.domainconverter.blacklist.domain.oneof.domain_class.BlacklistOneofBaseDomain;
import org.silbertb.proto.domainconverter.blacklist.domain.oneof.domain_field.BlacklistOneofFieldDomain;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.test.proto.blacklist.*;


import static org.junit.jupiter.api.Assertions.*;

public class BlacklistTest {
    @Test
    void testBlacklistToDomain() {
        BlacklistProto proto = BlacklistTestObjectsCreator.createBlacklistProto();
        BlacklistDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistDomain expected = BlacklistTestObjectsCreator.createBlacklistDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistToProto() {
        BlacklistDomain domain = BlacklistTestObjectsCreator.createBlacklistDomain();
        BlacklistProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistProto expected = BlacklistTestObjectsCreator.createBlacklistProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofStringToDomain() {
        BlacklistOneofFieldProto proto = BlacklistTestObjectsCreator.createBlacklistOneofFieldStringProto();
        BlacklistOneofFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofFieldDomain expected = BlacklistTestObjectsCreator.createBlacklistOneofFieldStringDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofStringToProto() {
        BlacklistOneofFieldDomain domain = BlacklistTestObjectsCreator.createBlacklistOneofFieldStringDomain();
        BlacklistOneofFieldProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofFieldProto expected = BlacklistTestObjectsCreator.createBlacklistOneofFieldStringProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofBooleanToDomain() {
        BlacklistOneofFieldProto proto = BlacklistTestObjectsCreator.createBlacklistOneofFieldBoolProto();
        BlacklistOneofFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofFieldDomain expected = BlacklistTestObjectsCreator.createBlacklistOneofFieldBoolDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofBooleanToProto() {
        BlacklistOneofFieldDomain domain = BlacklistTestObjectsCreator.createBlacklistOneofFieldBoolDomain();
        BlacklistOneofFieldProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofFieldProto expected = BlacklistTestObjectsCreator.createBlacklistOneofFieldBoolProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofBlacklistMsgToDomain() {
        BlacklistOneofFieldProto proto = BlacklistTestObjectsCreator.createBlacklistOneofFieldBlacklistMsgProto();
        BlacklistOneofFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofFieldDomain expected = BlacklistTestObjectsCreator.createBlacklistOneofFieldBlacklistMsgDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofBlacklistMsgToProto() {
        BlacklistOneofFieldDomain domain = BlacklistTestObjectsCreator.createBlacklistOneofFieldBlacklistMsgDomain();
        BlacklistOneofFieldProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofFieldProto expected = BlacklistTestObjectsCreator.createBlacklistOneofFieldBlacklistMsgProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofWhitelistMsgToDomain() {
        BlacklistOneofFieldProto proto = BlacklistTestObjectsCreator.createBlacklistOneofFieldWhitelistMsgProto();
        BlacklistOneofFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofFieldDomain expected = BlacklistTestObjectsCreator.createBlacklistOneofFieldWhitelistMsgDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofWhitelistMsgToProto() {
        BlacklistOneofFieldDomain domain = BlacklistTestObjectsCreator.createBlacklistOneofFieldWhitelistMsgDomain();
        BlacklistOneofFieldProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofFieldProto expected = BlacklistTestObjectsCreator.createBlacklistOneofFieldWhitelistMsgProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofInnerMsgToDomain() {
        BlacklistOneofFieldProto proto = BlacklistTestObjectsCreator.createBlacklistOneofFieldInnerMsgProto();
        BlacklistOneofFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofFieldDomain expected = BlacklistTestObjectsCreator.createBlacklistOneofFieldInnerMsgDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofInnerMsgToProto() {
        BlacklistOneofFieldDomain domain = BlacklistTestObjectsCreator.createBlacklistOneofFieldInnerMsgDomain();
        BlacklistOneofFieldProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofFieldProto expected = BlacklistTestObjectsCreator.createBlacklistOneofFieldInnerMsgProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBlacklistOneofBaseToDomain() {
        BlacklistOneofBaseProto proto = BlacklistTestObjectsCreator.createBlacklistOneofBaseProto();
        BlacklistOneofBaseDomain domain = ProtoDomainConverter.toDomain(proto);
        BlacklistOneofBaseDomain expected = BlacklistTestObjectsCreator.createBlacklistOneofBaseDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testBlacklistOneofBaseProto() {
        BlacklistOneofBaseDomain domain = BlacklistTestObjectsCreator.createBlacklistOneofBaseDomain();
        BlacklistOneofBaseProto proto = ProtoDomainConverter.toProto(domain);
        BlacklistOneofBaseProto expected = BlacklistTestObjectsCreator.createBlacklistOneofBaseProto();

        assertEquals(expected, proto);
    }
}
