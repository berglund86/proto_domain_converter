package org.silbertb.proto.domainconverter.custom_converter.domain;


import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.custom_converter.domain.wrapper.BooleanWrapperConverter;
import org.silbertb.proto.domainconverter.custom_converter.domain.wrapper.DoubleWrapperConverter;
import org.silbertb.proto.domainconverter.custom_converter.domain.wrapper.IntegerWrapperConverter;
import org.silbertb.proto.domainconverter.custom_converter.domain.wrapper.StringWrapperConverter;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.NullableFieldsExampleProto;

import java.util.UUID;

@Data
@ProtoClass(protoClass = NullableFieldsExampleProto.class)
public class NullableFieldsExampleDomain {

    @ProtoField
    @ProtoConverter(converter = UUIDConverter.class)
    private UUID id;

    @ProtoField
    @ProtoConverter(converter = StringWrapperConverter.class)
    private String name;

    @ProtoField
    @ProtoConverter(converter = StringWrapperConverter.class)
    private String description;

    @ProtoField
    @ProtoConverter(converter = StringWrapperConverter.class)
    private String address;

    @ProtoField
    @ProtoConverter(converter = IntegerWrapperConverter.class)
    private Integer age;

    @ProtoField
    @ProtoConverter(converter = DoubleWrapperConverter.class)
    private Double height;

    @ProtoField
    @ProtoConverter(converter = BooleanWrapperConverter.class)
    private Boolean active;

    @ProtoField
    @ProtoConverter(converter = DoubleWrapperConverter.class)
    private Double weight;
}
