package org.silbertb.proto.domainconverter.custom_converter.domain.wrapper;

import org.silbertb.proto.domainconverter.custom.TypeConverter;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.BooleanWrapper;

public class BooleanWrapperConverter implements TypeConverter<Boolean, BooleanWrapper> {
    @Override
    public Boolean toDomainValue(BooleanWrapper protoValue) {
        return protoValue.getValue();
    }

    @Override
    public boolean shouldAssignToProto(Boolean domainValue) {
        return domainValue != null;
    }

    @Override
    public BooleanWrapper toProtobufValue(Boolean domainValue) {
        return BooleanWrapper.newBuilder().setValue(domainValue).build();
    }
}