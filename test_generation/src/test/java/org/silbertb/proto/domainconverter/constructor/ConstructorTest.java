package org.silbertb.proto.domainconverter.constructor;

import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.constructor.domain.ConstructorDomain;
import org.silbertb.proto.domainconverter.constructor.domain.MixConstructorDomain;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.test.proto.constructor.ConstructorProto;
import org.silbertb.proto.domainconverter.test.proto.constructor.MixConstructorProto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class ConstructorTest {

    @Test
    void testConstructorToDomain() {
        ConstructorProto proto = ConstructorTestObjectsCreator.createConstructorProto();
        ConstructorDomain domain = ProtoDomainConverter.toDomain(proto);
        ConstructorDomain expected = ConstructorTestObjectsCreator.createConstructorDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testConstructorToProto() {
        ConstructorDomain domain = ConstructorTestObjectsCreator.createConstructorDomain();
        ConstructorProto proto = ProtoDomainConverter.toProto(domain);
        ConstructorProto expected = ConstructorTestObjectsCreator.createConstructorProto();

        assertEquals(expected, proto);
    }

    @Test
    void testMixConstructorToDomain() {
        MixConstructorProto proto = ConstructorTestObjectsCreator.createMixConstructorProto();
        MixConstructorDomain domain = ProtoDomainConverter.toDomain(proto);
        MixConstructorDomain expected = ConstructorTestObjectsCreator.createMixConstructorDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testMixConstructorToProto() {
        MixConstructorDomain domain = ConstructorTestObjectsCreator.createMixConstructorDomain();
        MixConstructorProto proto = ProtoDomainConverter.toProto(domain);
        MixConstructorProto expected = ConstructorTestObjectsCreator.createMixConstructorProto();

        assertEquals(expected, proto);
    }

    @Test
    void testMixConstructorUpdateDomain() {
        MixConstructorProto proto = MixConstructorProto.newBuilder().setIntVal(1).setStrVal("proto value").build();
        MixConstructorDomain domainUpdated = new MixConstructorDomain(22);
        domainUpdated.setStrVal("before update");
        MixConstructorDomain domainResult = ProtoDomainConverter.toDomain(proto, domainUpdated);
        assertSame(domainUpdated, domainResult);

        MixConstructorDomain expected = new MixConstructorDomain(22);
        expected.setStrVal("proto value");

        assertEquals(expected, domainUpdated);
    }
}
