package org.silbertb.proto.domainconverter.predefined_mappers;

import com.google.protobuf.Timestamp;
import org.silbertb.proto.domainconverter.custom.Mapper;

import java.time.Instant;

public class InstantTimestampMapper implements Mapper<Instant, Timestamp> {
    @Override
    public Instant toDomain(Timestamp timestamp) {
        return Instant.ofEpochSecond(timestamp.getSeconds(), timestamp.getNanos());
    }

    @Override
    public Timestamp toProto(Instant instant) {
        return Timestamp.newBuilder()
                .setSeconds(instant.getEpochSecond())
                .setNanos(instant.getNano())
                .build();
    }
}
