package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Accessors;
import org.silbertb.proto.domainconverter.util.StringUtils;

import java.util.List;

@Accessors(fluent = true)
@Value
public class ClassData implements DataWithDefault {
    String domainFullName;
    String domainClass;
    String protoFullName;
    String protoClass;
    String mapperClass;
    String mapperFullName;
    OneofBaseClassData oneofBaseClassData;
    List<FieldData> fieldsData;
    List<FieldData> constructorParameters;
    BuilderData builderData;
    boolean isDefault;

    @Builder
    public ClassData(String domainFullName,
                     String protoFullName,
                     String mapperFullName,
                     OneofBaseClassData oneofBaseClassData,
                     List<FieldData> fieldsData,
                     List<FieldData> constructorParameters,
                     BuilderData builderData,
                     boolean isDefault) {

        this.domainFullName = domainFullName;
        this.protoFullName = protoFullName;
        this.mapperFullName = mapperFullName;
        this.oneofBaseClassData = oneofBaseClassData;
        this.fieldsData = fieldsData;
        this.constructorParameters = constructorParameters;

        this.domainClass = StringUtils.getSimpleName(domainFullName);
        this.protoClass = StringUtils.getSimpleName(protoFullName);

        this.mapperClass = mapperFullName == null ? null : StringUtils.getSimpleName(mapperFullName);
        this.builderData = builderData;

        this.isDefault = isDefault;
    }

    public boolean useBuilder() {
        return builderData != null;
    }

    public boolean hasMapper() {
        return mapperClass != null;
    }

    public boolean hasSetters() {
        return fieldsData != null && !fieldsData.isEmpty() &&
                (builderData == null || builderData.useConstructorParams()) &&
                oneofBaseClassData == null;
    }

}
