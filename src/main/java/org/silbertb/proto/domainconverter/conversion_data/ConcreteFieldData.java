package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import org.silbertb.proto.domainconverter.util.StringUtils;
import org.silbertb.proto.domainconverter.custom.ProtoType;

@Accessors(fluent = true)
@Data
public class ConcreteFieldData {
    private final FieldType fieldType;
    private final String domainTypeFullName;
    private final String domainItemTypeFullName;
    private final String protoFieldPascalCase;
    private final String domainFieldMethodSuffix;
    private final String dataStructureConcreteType;
    private final String domainFieldName;

    private final String converterName;
    private final String converterFullName;
    private final ProtoType protoTypeForConverter;

    private final String generatedConverter;

    private final String explicitProtoFieldName;

    @Builder(toBuilder = true)
    public ConcreteFieldData(FieldType fieldType,
                             String domainItemTypeFullName,
                             String domainFieldName,
                             String domainTypeFullName,
                             String explicitProtoFieldName,
                             String dataStructureConcreteType,
                             String converterFullName,
                             ProtoType protoTypeForConverter,
                             String generatedConverter) {
        this.fieldType = fieldType;
        this.domainItemTypeFullName = domainItemTypeFullName;
        this.explicitProtoFieldName = explicitProtoFieldName;
        this.protoFieldPascalCase = protoFieldPascalCase(domainFieldName, explicitProtoFieldName);
        this.domainFieldMethodSuffix = StringUtils.capitalize(domainFieldName);
        this.dataStructureConcreteType = dataStructureConcreteType;
        this.converterFullName = converterFullName;
        this.converterName = converterFullName == null ? null : StringUtils.getSimpleName(converterFullName);
        this.protoTypeForConverter = protoTypeForConverter;
        this.domainFieldName = domainFieldName;
        this.domainTypeFullName = domainTypeFullName;
        this.generatedConverter = generatedConverter;
    }

    private static String protoFieldPascalCase(String domainFieldName, String customProtoFieldName) {
        return customProtoFieldName.equals("") ?
                StringUtils.capitalize(domainFieldName) :
                StringUtils.snakeCaseToPascalCase(customProtoFieldName);
    }

    private String generatedConverterPrefix() {
        return generatedConverter == null ? "" : generatedConverter + ".";
    }

    public boolean hasConverter() {
        return converterName != null;
    }

    public String protoBuilderSetCommand() {
        switch (protoTypeForConverter) {
            case MAP:
                return "putAll" + protoFieldPascalCase;
            case LIST:
                return "addAll" + protoFieldPascalCase;
            case OTHER:
            case MESSAGE:
                return "set" + protoFieldPascalCase;
            default:
                throw new RuntimeException("Unhandled proto type: " + fieldType);
        }
    }

    private String domainGetterMethodPrefix() {
        if (fieldType.equals(FieldType.BOOLEAN)) {
            return "is";
        }

        return "get";
    }

    public String domainGetterMethod() {
        return domainGetterMethodPrefix() + domainFieldMethodSuffix;
    }

    private String protoGetterSuffix() {
        switch (protoTypeForConverter) {
            case LIST:
                return "List";
            case MAP:
                return "Map";
            case OTHER:
            case MESSAGE:
                return "";
            default:
                throw new RuntimeException("Unhandled proto type: " + protoTypeForConverter);
        }
    }

    public String protoGetterMethod() {
        return "get" + protoFieldPascalCase + protoGetterSuffix();
    }

    public boolean isNullableDomainType() {
        switch (fieldType) {
            case MESSAGE:
            case ENUM:
            case PRIMITIVE_LIST:
            case MESSAGE_LIST:
            case PRIMITIVE_MAP:
            case MAP_TO_MESSAGE:
            case STRING:
            case BYTES:
                return true;
            case OTHER:
            case BOOLEAN:
                return false;
            default:
                throw new RuntimeException("Unhandled field type: " + fieldType);
        }
    }

    public String addToBuilderCommand() {
        switch (fieldType) {
            case BOOLEAN:
                return "builder.set" + protoFieldPascalCase + "(domain.is" + domainFieldMethodSuffix + "())";
            case MESSAGE:
            case ENUM:
                return "builder.set" + protoFieldPascalCase + "(" + generatedConverterPrefix() + "toProto(domain.get" + domainFieldMethodSuffix + "()))";
            case PRIMITIVE_LIST:
                return "builder.addAll" + protoFieldPascalCase + "(domain.get" + domainFieldMethodSuffix + "())";
            case MESSAGE_LIST:
                return "domain.get" + domainFieldMethodSuffix + "().forEach(item -> builder.add" + protoFieldPascalCase + "(" + generatedConverterPrefix() + "toProto(item)))";
            case PRIMITIVE_MAP:
                return "builder.putAll" + protoFieldPascalCase + "(domain.get" + domainFieldMethodSuffix + "())";
            case MAP_TO_MESSAGE:
                return "domain.get" + domainFieldMethodSuffix + "().forEach((key, value) -> builder.put" + protoFieldPascalCase + "(key, " + generatedConverterPrefix() + "toProto(value)))";
            case BYTES:
                return "builder.set" + protoFieldPascalCase + "(ByteString.copyFrom(domain.get" + domainFieldMethodSuffix + "()))";
            case STRING:
            case OTHER:
                return "builder.set" + protoFieldPascalCase + "(domain.get" + domainFieldMethodSuffix + "())";
            default:
                throw new RuntimeException("Unhandled field type: " + fieldType);
        }
    }

    public boolean isNullableProtoType() {
        return fieldType == FieldType.MESSAGE || protoTypeForConverter == ProtoType.MESSAGE;
    }

    public String convertProtoValue() {
        switch (fieldType) {
            case BOOLEAN:
            case STRING:
            case OTHER:
                return "proto.get" + protoFieldPascalCase + "()";
            case PRIMITIVE_MAP:
                return "new " + dataStructureConcreteType + "<>(proto.get" + protoFieldPascalCase + "())";
            case MESSAGE:
            case ENUM:
                return generatedConverterPrefix() + "toDomain(proto.get" + protoFieldPascalCase + "(), (" + domainTypeFullName + ") null)";
            case PRIMITIVE_LIST:
                return "new " + dataStructureConcreteType + "<>(proto.get" + protoFieldPascalCase + "List())";
            case MESSAGE_LIST:
                return "proto.get" + protoFieldPascalCase + "List().stream().map(item -> " + generatedConverterPrefix() + "toDomain(item, (" + domainItemTypeFullName + ") null)).collect(Collectors.toCollection(" + dataStructureConcreteType + "::new))";
            case MAP_TO_MESSAGE:
                return "proto.get" + protoFieldPascalCase + "Map().entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> " + generatedConverterPrefix() + "toDomain(e.getValue(), (" + domainItemTypeFullName + ") null), (v1, v2) -> v1, " + dataStructureConcreteType + "::new))";
            case BYTES:
                return "proto.get" + protoFieldPascalCase + "().toByteArray()";
            default:
                throw new RuntimeException("Unhandled field type: " + fieldType);
        }
    }

    public boolean isMessage() {
        return fieldType.equals(FieldType.MESSAGE);
    }
}
