package org.silbertb.proto.domainconverter.converter.oneof_field;

import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.custom.NullConverter;
import org.silbertb.proto.domainconverter.custom.ProtoType;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;

import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;

public class OneofConverterUtil {

    private final LangModelUtil langModelUtil;
    private final ProtoTypeUtil protoTypeUtil;

    public OneofConverterUtil(LangModelUtil langModelUtil, ProtoTypeUtil protoTypeUtil) {
        this.langModelUtil = langModelUtil;
        this.protoTypeUtil = protoTypeUtil;
    }

    public String getTypeConverterName(OneofField oneofFieldAnnotation, Element elementPossiblyAnnotatedByProtoConverter) {
        TypeMirror converterType = getTypeConverter(oneofFieldAnnotation, elementPossiblyAnnotatedByProtoConverter);
        return converterType == null ? null : converterType.toString();
    }
    
    public TypeMirror getTypeConverter(OneofField oneofFieldAnnotation, Element elementPossiblyAnnotatedByProtoConverter) {
        @SuppressWarnings("ResultOfMethodCallIgnored")
        TypeMirror converterType = langModelUtil.getClassFromAnnotation(oneofFieldAnnotation::converter);
        if(isNullConverter(converterType)) {
            ProtoConverter protoConverterAnnotation = elementPossiblyAnnotatedByProtoConverter.getAnnotation(ProtoConverter.class);
            if(protoConverterAnnotation == null) {
                return null;
            }
            //noinspection ResultOfMethodCallIgnored
            converterType = langModelUtil.getClassFromAnnotation(protoConverterAnnotation::converter);
        }
        validateConverter(oneofFieldAnnotation, converterType);

        return converterType;
    }

    public void validateConverter(OneofField oneofFieldAnnotation, TypeMirror converterType) {
        ProtoType protoType = protoTypeUtil.getProtoTypeFromConverter(converterType);
        if(protoType != ProtoType.OTHER && protoType != ProtoType.MESSAGE) {
            throw new RuntimeException("protoType for @ProtoConverter on a 'oneof' field must be 'OTHER'. oneof base field: " +
                    oneofFieldAnnotation.protoField() +
                    " class: " + oneofFieldAnnotation.domainClass() +
                    " field: " + oneofFieldAnnotation.domainField());
        }
    }

    public boolean isNullConverter(OneofField oneofFieldAnnotation) {
        @SuppressWarnings("ResultOfMethodCallIgnored")
        TypeMirror converterClass = langModelUtil.getClassFromAnnotation(oneofFieldAnnotation::converter);
        return converterClass.toString().equals(NullConverter.class.getName());
    }

    public boolean isNullConverter(TypeMirror converterClass) {
        return converterClass.toString().equals(NullConverter.class.getName());
    }
}
