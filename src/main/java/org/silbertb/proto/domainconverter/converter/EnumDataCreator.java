package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.ProtoClassDefault;
import org.silbertb.proto.domainconverter.annotations.ProtoEnum;
import org.silbertb.proto.domainconverter.annotations.ProtoEnumValue;
import org.silbertb.proto.domainconverter.conversion_data.EnumData;
import org.silbertb.proto.domainconverter.conversion_data.EnumValueData;
import org.silbertb.proto.domainconverter.util.LangModelUtil;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class EnumDataCreator {

    private final LangModelUtil langModelUtil;
    private final ProcessingEnvironment processingEnv;
    private final ConverterLogger logger;
    public EnumDataCreator(LangModelUtil langModelUtil,
                           ProcessingEnvironment processingEnv,
                           ConverterLogger logger) {
        this.langModelUtil = langModelUtil;
        this.processingEnv = processingEnv;
        this.logger = logger;
    }

    public EnumData createEnumData(TypeElement domainElement) {
        ProtoEnum protoEnumAnnotation = domainElement.getAnnotation(ProtoEnum.class);

        @SuppressWarnings("ResultOfMethodCallIgnored")
        TypeMirror protoEnum = langModelUtil.getClassFromAnnotation(protoEnumAnnotation::protoEnum);

        Set<String> protoValueNames = getEnumValues(processingEnv.getTypeUtils().asElement(protoEnum))
                .stream()
                .map(Object::toString)
                .collect(Collectors.toSet());

        List<EnumValueData> enumValueDataList = getEnumValueDataForAnnotatedValues(domainElement);
        addDefaultEnumValueDataForUnAnnotatedValues(protoValueNames, enumValueDataList, protoEnumAnnotation.mapUnrecognized());

        String domainFullName = domainElement.getQualifiedName().toString();
        String protoFullName = protoEnum.toString();

        EnumData.EnumDataBuilder enumDataBuilder = EnumData.builder()
                .domainFullName(domainFullName)
                .protoFullName(protoFullName)
                .enumValueData(enumValueDataList);

        ProtoClassDefault protoClassDefaultAnnotation = domainElement.getAnnotation(ProtoClassDefault.class);
        if(protoClassDefaultAnnotation != null) {
            enumDataBuilder.isDefault(true);
        }
        return enumDataBuilder.build();
    }

    private List<? extends Element> getEnumValues(Element enumTypeElement) {
        if(enumTypeElement.getKind() != ElementKind.ENUM) {
            logger.error(enumTypeElement + " is not of ENUM kind");
        }

        return enumTypeElement.getEnclosedElements().stream()
                .filter(element -> element.getKind().equals(ElementKind.ENUM_CONSTANT))
                .collect(Collectors.toList());
    }

    private List<EnumValueData> getEnumValueDataForAnnotatedValues(TypeElement domainElement) {
        List<EnumValueData> enumValueDataList = new ArrayList<>();
        for(Element domainEnumValue : getEnumValues(domainElement)) {
            ProtoEnumValue protoEnumValueAnnotation = domainEnumValue.getAnnotation(ProtoEnumValue.class);
            if(protoEnumValueAnnotation != null) {
                EnumValueData enumValueData = EnumValueData.builder()
                        .domainName(domainEnumValue.toString())
                        .protoName(protoEnumValueAnnotation.protoEnum())
                        .build();
                enumValueDataList.add(enumValueData);
            }
        }
        return enumValueDataList;
    }

    private void addDefaultEnumValueDataForUnAnnotatedValues(
            Set<String> protoValueNames,
            List<EnumValueData> enumValueDataList,
            boolean mapUnrecognized) {
        for(EnumValueData enumValueData : enumValueDataList) {
            protoValueNames.remove(enumValueData.getProtoName());
        }

        if(!mapUnrecognized) {
            protoValueNames.remove("UNRECOGNIZED");
        }

        for(String protoValueName : protoValueNames) {
            EnumValueData enumValueData = EnumValueData.builder()
                    .domainName(protoValueName)
                    .protoName(protoValueName)
                    .build();
            enumValueDataList.add(enumValueData);
        }
    }

}
