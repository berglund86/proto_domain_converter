package org.silbertb.proto.domainconverter;

import org.silbertb.proto.domainconverter.conversion_data.ConversionData;
import org.silbertb.proto.domainconverter.converter.ConversionDataCreator;
import org.silbertb.proto.domainconverter.converter.ConverterLogger;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.io.IOException;
import java.util.Set;

@SupportedAnnotationTypes({
        "org.silbertb.proto.domainconverter.annotations.ProtoClass",
        "org.silbertb.proto.domainconverter.annotations.ProtoEnum",
        "org.silbertb.proto.domainconverter.annotations.ProtoGlobalMapper",
        "org.silbertb.proto.domainconverter.annotations.ProtoConfigure"
})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class ConverterGenerator extends AbstractProcessor {
    private SourceWriter sourceWriter;
    private ConverterLogger logger;
    private ConversionDataCreator conversionDataCreator;

    private int numberOfCalls = 0;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        sourceWriter = new SourceWriter(processingEnv);

        logger = new ConverterLogger(processingEnv);

        conversionDataCreator = new ConversionDataCreator(processingEnv, logger);
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if(numberOfCalls > 0) {
            //Only the first call to this processor is processed.
            //This processor always writes to the same file and no additional annotations handled by this processor
            //are expected in the generated code
            return true;
        }
        numberOfCalls++;

        ConversionData conversionData = conversionDataCreator.createConversionData(roundEnv);
        if(conversionData.classesData().isEmpty() && conversionData.enumData().isEmpty()) {
            return true;
        }

        // Write source file
        try {
            sourceWriter.writeSource(conversionData);
        } catch (IOException ex) {
            logger.error("Failed to generate proto domain conversion class. Exception: " + ex);
            throw new RuntimeException(ex);
        }

        return true;
    }
}
