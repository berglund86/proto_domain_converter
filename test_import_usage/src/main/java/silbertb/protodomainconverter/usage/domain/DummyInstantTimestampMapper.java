package silbertb.protodomainconverter.usage.domain;

import com.google.protobuf.Timestamp;
import org.silbertb.proto.domainconverter.annotations.ProtoGlobalMapper;
import org.silbertb.proto.domainconverter.custom.Mapper;

import java.time.Instant;

@ProtoGlobalMapper
public class DummyInstantTimestampMapper implements Mapper<Instant, Timestamp> {
    @Override
    public Instant toDomain(Timestamp timestamp) {
        return Instant.ofEpochSecond(timestamp.getSeconds(), timestamp.getNanos()+1);
    }

    @Override
    public Timestamp toProto(Instant instant) {
        return Timestamp.newBuilder()
                .setSeconds(instant.getEpochSecond())
                .setNanos(instant.getNano()-1)
                .build();
    }
}
