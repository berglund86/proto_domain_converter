package silbertb.protodomainconverter.dep1.domain;

import com.google.protobuf.Timestamp;
import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.model.proto.*;
import silbertb.protodomainconverter.dep2.domain.Dep2BuilderDomain;
import silbertb.protodomainconverter.dep2.domain.Dep2ConstructorBuilderDomain;
import silbertb.protodomainconverter.dep2.domain.Dep2ConstructorDomain;
import silbertb.protodomainconverter.dep2.domain.Dep2PojoDomain;
import silbertb.protodomainconverter.usage.UsageConverter;
import silbertb.protodomainconverter.usage.domain.TimestampDomain;
import silbertb.protodomainconverter.usage.domain.UsageDomain;

import java.time.Instant;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UsageDomainTest {

    @Test
    public void testToProto() {
        UsageDomain domain = createUsageDomain();
        UsageProto proto = UsageConverter.toProto(domain);
        UsageProto expected = createUsageProto();

        assertEquals(expected, proto);
    }

    @Test
    public void testToDomain() {
        UsageProto proto = createUsageProto();
        UsageDomain domain = UsageConverter.toDomain(proto);
        UsageDomain expected = createUsageDomain();

        assertEquals(expected, domain);
    }

    @Test
    public void testThatGlobalMapperInProjectOverrulesImportedGlobalMapper() {
        TimestampDomain domain = TimestampDomain.builder()
                .timestamp(Instant.ofEpochSecond(1L,1L))
                .build();
        TimestampProto proto = UsageConverter.toProto(domain);
        TimestampProto expected = TimestampProto.newBuilder()
                .setTimestamp(Timestamp.newBuilder().setSeconds(1).setNanos(0)) //The mapper in the project reduce 1 nano
                .build();

        assertEquals(expected, proto);
    }

    private UsageProto createUsageProto() {
        return UsageProto.newBuilder()
                .setDoubleValue(3.3)
                .setDep2Pojo(Dep2PojoProto.newBuilder()
                        .setIntValue(3)
                        .setDep1(Dep1Proto.newBuilder().setStringValue("single"))
                        .addDep1List(Dep1Proto.newBuilder().setStringValue("item1"))
                        .addDep1List(Dep1Proto.newBuilder().setStringValue("item2"))
                        .putDep1Map("key1", Dep1Proto.newBuilder().setStringValue("val1").build())
                        .putDep1Map("key2", Dep1Proto.newBuilder().setStringValue("val2").build()))
                .setDep2Constructor(Dep2ConstructorProto.newBuilder()
                        .setDep1(Dep1Proto.newBuilder().setStringValue("dep1Constructor")))
                .setDep2Builder(Dep2BuilderProto.newBuilder()
                        .setDep1(Dep1Proto.newBuilder().setStringValue("dep1Builder")))
                .setDep2BuilderConstructor(Dep2ConstructorBuilderProto.newBuilder()
                        .setDep1(Dep1Proto.newBuilder().setStringValue("dep1BuilderConstructor")))
                .setDep1(Dep1Proto.newBuilder().setStringValue("dep1"))
                .build();
    }

    private UsageDomain createUsageDomain() {
        Dep2PojoDomain dep2Pojo = new Dep2PojoDomain();
        dep2Pojo.setIntValue(3);
        dep2Pojo.setDep1(Dep1Domain.builder().stringValue("single").enumValue(Dep1Domain.Dep1Enum.E1).build());
        dep2Pojo.setDep1List(List.of(
                Dep1Domain.builder().stringValue("item1").enumValue(Dep1Domain.Dep1Enum.E1).build(),
                Dep1Domain.builder().stringValue("item2").enumValue(Dep1Domain.Dep1Enum.E1).build()
        ));
        dep2Pojo.setDep1Map(Map.of(
                "key1", Dep1Domain.builder().stringValue("val1").enumValue(Dep1Domain.Dep1Enum.E1).build(),
                "key2", Dep1Domain.builder().stringValue("val2").enumValue(Dep1Domain.Dep1Enum.E1).build()
        ));


        return UsageDomain.builder()
                .doubleValue(3.3)
                .dep2Pojo(dep2Pojo)
                .dep2Constructor(new Dep2ConstructorDomain(Dep1Domain.builder().stringValue("dep1Constructor").enumValue(Dep1Domain.Dep1Enum.E1).build()))
                .dep2Builder(Dep2BuilderDomain.builder()
                        .dep1(Dep1Domain.builder().stringValue("dep1Builder").enumValue(Dep1Domain.Dep1Enum.E1).build())
                        .build())
                .dep2BuilderConstructor(Dep2ConstructorBuilderDomain.builder()
                        .dep1(Dep1Domain.builder().stringValue("dep1BuilderConstructor").enumValue(Dep1Domain.Dep1Enum.E1).build())
                        .build())
                .dep1(Dep1Domain.builder().stringValue("dep1").enumValue(Dep1Domain.Dep1Enum.E1).build())
                .build();
    }

}